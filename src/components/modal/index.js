import React from 'react'
import {StyledModal, ModalContent, StyledClose} from "./styled"
import PropTypes from "prop-types";
import Button from "../button"
const config = {
  modalAddToCart: {
    id: "modalAddToCart",
    title: "Add to cart",
    text: "Do you want add to cart?",
    isCloseBtn: true,
    
  }, modalDelete:{
    id: "modalDelete",
    title: "modal delete",
    text: "Do you want delete from cart?",
    isCloseBtn: true,
  }
}
const Modal = ({idModal, currentId, onSubmit, onClose}) => {
const {isCloseBtn, title, text} = config[idModal]
  return (
    <StyledModal onClick = {()=>{onClose()}} >
                <ModalContent onClick={e => e.stopPropagation()} >
                    {isCloseBtn && <StyledClose  onClick = {()=>{onClose()}} >&times;</StyledClose>}
                    <h2>{title}</h2>
                    <p>{text}</p>
                    <Button text="Submit" styles="yellow" onClick={() => { onSubmit(currentId)}} />,
                    <Button text="Cancel" styles="purple" onClick={() => { onClose() }} />
                </ModalContent>
            </StyledModal>
  )
}
Modal.propTypes = {
  title: PropTypes.string,
  children: PropTypes.string,
  id: PropTypes.string,
  onClose: PropTypes.func,
  addProductToCart: PropTypes.func,
  isCloseBtn: PropTypes.bool,
  text: PropTypes.string,
  action: PropTypes.string,
}
export default Modal