import styled from "styled-components" 
export const StyledBtn = styled.button`
background-color: ${props => props.$bgStyle || "#BF4F74"};
border: none;
padding: 20px;
border-radius: 5px;
margin-right: 20px;
`
