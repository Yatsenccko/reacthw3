import React from 'react'
import {StyledBtn} from "./styled"
const Button = (props) => {
  const {
    text, onClick, styles, productId
} = props
  return (
    <StyledBtn onClick={()=> onClick( productId)} $bgStyle = {styles} >{text}</StyledBtn>
  )
}

export default Button