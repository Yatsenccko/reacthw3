import React from 'react'
import { AiOutlineShoppingCart, AiOutlineHeart, AiFillHome } from 'react-icons/ai';
import { Link } from 'react-router-dom';
import { StyledHeader } from "./styled";
const Header = ({
  fav,
  countCart
}) => {
  const elementsInCart = countCart.reduce((acc, current) => {
    acc += current.count
    return acc
  }, 0 )
  return (


    <StyledHeader>

      <Link to="/">
      <AiFillHome />
      </Link>

      <Link to="/favorite">
      <AiOutlineHeart />{fav}
      </Link>

      <Link to="/cart">
      <AiOutlineShoppingCart />
      {elementsInCart}
      </Link>

    </StyledHeader>
  )

}

export default Header