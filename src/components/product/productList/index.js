import React from 'react'
import ProductCart from '../productCart'
export const ProductList = ({products, addProductToFav, modalOpen, button, cross, openDeleteModal }) => {
  return (
    <ul>
            {products.map(product => (
                <ProductCart key = {product.id} {...product} addProductToFav = {addProductToFav} openDeleteModal={openDeleteModal} modalOpen = {modalOpen} button={button} cross={cross} />
            ))}
        </ul>
    
  )
}
