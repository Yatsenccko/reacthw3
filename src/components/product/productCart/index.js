import React from 'react'
import { StyledImg, StyledCard, StyledButton } from "./styled"
import { AiFillHeart, AiOutlineHeart, } from 'react-icons/ai';
import { RxCross2 } from 'react-icons/rx';
import Button from "../../button";
const ProductCart = (props) => {
  const {
    title,
    imgSrc,
    price,
    article,
    color,
    fav,
    id,
    addProductToFav,
    modalOpen,
    cross,
    button,
    openDeleteModal
  } = props
  return (

    <StyledCard>
      {cross && <RxCross2 onClick={() => openDeleteModal(id)} />}
      <p>{fav ? <AiFillHeart onClick={() => addProductToFav(id)} /> : <AiOutlineHeart onClick={() => addProductToFav(id)} />}</p>

      <h3>{title}</h3>
      <StyledImg src={imgSrc} />
      <p>{price}</p>
      <p>{article}</p>
      <p>{color}</p>
      {button && <Button text="Add to cart" styles="blue" id="modalAddToCart" onClick={() => modalOpen(id)} />}
    </StyledCard>
  )
}

export default ProductCart