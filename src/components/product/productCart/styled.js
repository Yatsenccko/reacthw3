import styled from "styled-components" 
export  const StyledImg = styled.img`
width: 400px;
height: 400px;
`
export const StyledCard = styled.li`
    width: 450px;
    list-style: none;
    background-color: white;
    border: none;
    border-radius:10px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 20px;
    box-shadow: 10px 10px 20px 5px #808080;
    margin-bottom: 40px;

`