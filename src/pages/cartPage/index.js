import React, { useEffect, useState } from 'react'
import { ProductList } from '../../components/product/productList'
const CartPage = ({products, addProductToFav, openDeleteModal }) => {
  const [productInCart, setProductInCart] = useState(products)
  useEffect(()=>{
     const filtered = products.filter(item => item.cart)
     setProductInCart(filtered)
  }, [products] )
  console.log(products, productInCart);
  return (
    <ProductList products={productInCart} addProductToFav={addProductToFav} openDeleteModal={openDeleteModal} cross = {true} button={false}/>
  )
}

export default CartPage