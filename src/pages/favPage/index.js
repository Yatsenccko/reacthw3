
import React, { useEffect, useState } from 'react'
import { ProductList } from '../../components/product/productList'
const FavPage = ({products, addProductToFav, modalOpen}) => {
  const [productInFav, setProductInFav] = useState(products)
  useEffect(()=>{
     const filtered = products.filter(item => item.fav)
     setProductInFav(filtered)
  }, [products] )
  console.log(products, productInFav);
  return (
    <ProductList products={productInFav} addProductToFav={addProductToFav} modalOpen={modalOpen} button={true} cross={false} />
  )
}

export default FavPage