import React, {useEffect, useState} from 'react'
import { Routes, Route } from 'react-router-dom'
import Header from '../../components/header'
import HomePage from '../homePage'
import FavPage from '../favPage'
import CartPage from '../cartPage'
import Modal from '../../components/modal'
const LayOut = () => {
    const [products,setProducts] = useState([])
    // useState
    const [isOpen, setModalOpen] = useState(false)
    const [currentId, setCurrentId] = useState(null)
    const [countFav, setCountFav] = useState(JSON.parse(localStorage.getItem("fav"))?.length || 0) 
    const [countCart, setCountCart] = useState(JSON.parse(localStorage.getItem("cart")) || [])
    const [isOpenDelete, setIsOpenDelete] = useState(false)
    useEffect(()=>{
        (async()=>{
            const res = await fetch('db.json') 
            const data = await res.json()
            const storage = createStorage()
            const checkProducts = checkInFavAndInCart(data.products, storage)
            setProducts(checkProducts)
        })()
    }, [])
    function openDeleteModal(id) {
      setCurrentId(id)
      setIsOpenDelete(true)
    } 
    function createStorage() {
        const cart = JSON.parse(localStorage.getItem("cart"))
        const fav = JSON.parse(localStorage.getItem("fav"))
        if (!cart || !fav) {
          localStorage.setItem("cart", JSON.stringify([]))
          localStorage.setItem("fav", JSON.stringify([]))
        }
        return {
          fav, cart
        }
      }
      function checkInFavAndInCart(products, { fav, cart }) {
        const idInCart = cart.map((item)=> item.id)
                const updateInCart = products.map(item => {
          if (idInCart.includes(item.id)) {
            item.cart = true
            return item
          }
        
          
          return item
        })
        
        const updateInFav = updateInCart.map((item)=>{
          if (fav.includes(item.id)) {
            item.fav = true
            
            return item
          }
          return item
        })
        return updateInFav
    
      }
      
      function  addProductToFav(id) {
        const storage = JSON.parse(localStorage.getItem("fav"))
        const finded = storage.find(item => item === id)
        if (!finded) {
          const newStorage = [...storage, id]
          localStorage.setItem("fav", JSON.stringify(newStorage))
          setCountFav((prev)=> prev += 1)
          const productInFav = products.map((item)=>{
            if (item.id === id) {
                    item.fav = true
                    return item
                  }
                  return item
          })
          setProducts(productInFav)
          return
        }
        if (finded) {
          const newStorage = storage.filter(item => item !== id)
          localStorage.setItem("fav", JSON.stringify(newStorage))
          setCountFav((prev)=> prev -= 1)
          const productInFav = products.map((item)=>{
            if (item.id === id) {
                    item.fav = false
                    return item
                  }
                  return item
          })
          setProducts(productInFav)
          return
    
        }
      }
    function modalOpen(id){
      setModalOpen(!isOpen)
      setCurrentId(id)
    }
     function addProductToCart(currentId) {
        const storage = JSON.parse(localStorage.getItem("cart"))
        const finded = storage.find(item => item.id === currentId)
       
        if (!finded) {
          const newStorage = [...storage, {id: currentId, count: 1 }]
          localStorage.setItem("cart", JSON.stringify(newStorage))
            const productInCart = products.map(item => {
              if (item.id === currentId) {
                item.cart = true
                return item
              }
              return item
            })
            setProducts(productInCart)
            setCountCart(newStorage)
            setModalOpen(!isOpen)
            setCurrentId(null)
            
          return
        }
        if (finded) {
          let count = finded.count + 1
          
          let findedIndex = storage.findIndex(item => item.id === currentId)
          const newStorage =  storage.toSpliced(findedIndex, 1, {
            id: currentId, count
          })
          localStorage.setItem("cart", JSON.stringify(newStorage))
          
            setCountCart(newStorage)
            setModalOpen(!isOpen)
            setCurrentId(null) 
        }
      }    
      function hideModal(){
        setCurrentId(null)
        setIsOpenDelete(false)
        setModalOpen(false)
      }
      function deleteCart(id) {
        const storage = JSON.parse(localStorage.getItem("cart"))
        const newStorage = storage.filter(item=>item.id!==id)
        localStorage.setItem("cart", JSON.stringify(newStorage))
        const filteredInStorage = countCart.filter(item=> item.id !== id) 
        setCountCart(filteredInStorage, 222)
        const newProducts = products.map((item)=> {
          if(item.id === id){
            item.cart = false
            return item
          }
          return item
        })
        setProducts(newProducts)
        hideModal()
      }
    return (
        <div>
        <Header fav = {countFav} countCart={countCart}/>
            <Routes>
                <Route path="/" element={<HomePage products = {products} addProductToFav = {addProductToFav} modalOpen={modalOpen}/>} /> 
                <Route path="/favorite" element={<FavPage  products = {products} addProductToFav= {addProductToFav} modalOpen={modalOpen}/>} /> 
                <Route path="/cart" element={<CartPage  products = {products} addProductToFav= {addProductToFav} openDeleteModal={openDeleteModal} />} /> 
            </Routes>
            {isOpen && <Modal idModal = 'modalAddToCart'  currentId = {currentId}  onSubmit = {addProductToCart}  onClose = {hideModal}/>}
            {isOpenDelete && <Modal idModal = 'modalDelete'  currentId = {currentId} onSubmit={deleteCart}   onClose = {hideModal}/>}
        </div>

    )
}

export default LayOut