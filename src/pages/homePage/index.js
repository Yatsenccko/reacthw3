import React from 'react'
import {ProductList} from '../../components/product/productList'
const HomePage = ({products, addProductToFav, modalOpen}) => {
  console.log(products, 1);
  return (
    <ProductList products={products} addProductToFav = {addProductToFav} modalOpen={modalOpen} button={true} cross={false}/>
  )
}

export default HomePage