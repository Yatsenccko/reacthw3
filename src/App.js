import PropTypes from 'prop-types';
import logo from './logo.svg';
import LayOut from './pages/layout';
import './App.css';

function App() {
  return (
    <div className="App">
      <LayOut/>
      
    </div>
  );
}

export default App;
